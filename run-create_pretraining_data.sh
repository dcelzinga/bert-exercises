BERT_HOME=$HOME/bert
python $BERT_HOME/create_pretraining_data.py \
  --input_file=guten-902-sents.txt \
  --output_file=guten-902-sents.tfrecord \
  --vocab_file=$BERT_BASE_DIR/vocab.txt \
  --do_lower_case=True \
  --max_seq_length=128 \
  --max_predictions_per_seq=20 \
  --masked_lm_prob=0.15 \
  --random_seed=12345 \
  --dupe_factor=5
